package uk.co.exus.koutroulis.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ch.koutroulis on 05-Jul-17.
 */

@Service
public class ProductService {
    public List<String> getProducts() {
       return Arrays.asList("Samsung Galaxy S7 Edge", "iPhone 7 Plus", "OnePlus 5");
    }
}
