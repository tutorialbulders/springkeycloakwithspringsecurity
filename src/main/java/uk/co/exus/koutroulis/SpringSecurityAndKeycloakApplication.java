package uk.co.exus.koutroulis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityAndKeycloakApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityAndKeycloakApplication.class, args);
    }

}
