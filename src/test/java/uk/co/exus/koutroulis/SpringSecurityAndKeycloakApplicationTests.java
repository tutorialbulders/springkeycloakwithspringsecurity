package uk.co.exus.koutroulis;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringSecurityAndKeycloakApplicationTests {

	@Test
	public void contextLoads() {
	}

}
